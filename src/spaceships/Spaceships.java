package spaceships;

public class Spaceships
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        MainWindow window = new MainWindow();
        window.setVisible(true);
        Game.init(window.getCanvas());
        Thread myGame = new Thread(Game.getInstance());
        myGame.start();
    }

}
