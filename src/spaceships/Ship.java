package spaceships;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public abstract class Ship extends Rectangle
{

    private final Color _color;
    private final Image _image;

    /**
     *
     * Crear una nueva instancia de nave.
     *
     * @param x         Posición inicial en el eje X.
     * @param y         Posición inicial en el eje Y.
     * @param width     Ancho de la nave en pixeles.
     * @param height    Alto de la nave en pixeles.
     * @param color     Color con el que se dibujará la nave.
     * @param imageName Nombre de la imagen que se dibujará.
     *
     */
    public Ship(int x, int y, int width, int height, Color color,
                String imageName)
    {
        super(x, y, width, height);
        _color = color;
        _image = ContentManager.getImage(imageName);
    }

    /**
     *
     * Dibujar la nave en el canvas.
     *
     * @param g Elemento "Graphics" del canvas sobre el cual se dibujará.
     *
     */
    public void draw(Graphics g)
    {
        if (_image == null)
        {
            g.setColor(_color);
            g.fillRect(getX(), getY(), 50, 50);
        }
        else
        {
            g.drawImage(_image, getX(), getY(), getWidth(), getHeight(), null);
        }
    }

    /**
     *
     * Actualizar el estado de la nave.
     *
     */
    public abstract void update();
}
