package spaceships;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;

public class ContentManager
{

    /**
     *
     * Obtener una imagen desde la carpeta de recursos.
     *
     * @param fileName Nombre de la imagen a cargar.
     *
     * @return Imagen desde la carpeta de recursos correspondiente al nombre
     *         dado.
     *
     */
    public static Image getImage(String fileName)
    {
        File file = new File("res/Images/" + fileName);
        if (file.exists() && !file.isDirectory())
        {
            return new ImageIcon("res/Images/" + fileName).
                    getImage();
        }
        return null;
    }

    /**
     *
     * Obtener una imagen desde la carpeta de recursos.
     *
     * @param fileName Nombre de la imagen a cargar.
     *
     * @return Imagen desde la carpeta de recursos correspondiente al nombre
     *         dado.
     *
     */
    public static AudioInputStream getSoundClip(String fileName)
    {
        try
        {
            File file = new File("res/Sounds/" + fileName);
            if (file.exists() && !file.isDirectory())
            {
                return AudioSystem.getAudioInputStream(
                        new File("res/Sounds/" + fileName));
            }
        }
        catch (IOException | UnsupportedAudioFileException ex)
        {
            ; // Ignored
        }
        return null;
    }
}
