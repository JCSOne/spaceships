package spaceships;

import java.awt.Canvas;
import java.awt.Color;
import java.util.ArrayList;

public class Game implements Runnable
{

    private final Canvas _canvas;
    private final ArrayList<Enemy> _enemies;
    private final KeyboardInput _keyboardInput;
    private final Player _player;
    private static Game _game;
    private static Thread _soundThread;

    // Usadas para determinar el intervalo de refrezco del juego
    // Tomado de: https://stackoverflow.com/a/14421479
    double interpolation = 0;
    final int TICKS_PER_SECOND = 25;
    final int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
    final int MAX_FRAMESKIP = 5;

    /**
     *
     * Obtener la instancia del juego.
     *
     * @return El juego.
     *
     */
    public static Game getInstance()
    {
        return _game;
    }

    /**
     *
     * Inicializar la instancia del juego.
     *
     * @param canvas Canvas sobre el cual se dibujaran los elementos del juego.
     *
     */
    public static void init(Canvas canvas)
    {
        _game = new Game(canvas);
    }

    /**
     *
     * Crear una nueva instancia de juego.
     *
     * @param canvas Canvas sobre el cual se dibujaran los elementos del juego.
     *
     */
    private Game(Canvas canvas)
    {
        _canvas = canvas;
        _player = new Player(0, _canvas.getHeight() - 46, 46, 46, Color.BLUE,
                             "player.png");
        _keyboardInput = new KeyboardInput();
        _canvas.addKeyListener(_keyboardInput);
        _enemies = new ArrayList<>();
        _enemies.add(new Enemy(0, 0, 46, 46, Color.RED, "enemy.png"));
        _enemies.add(new Enemy(150, 0, 46, 46, Color.YELLOW, "enemy.png"));
    }

    /**
     *
     * Dibujar los elementos del juego.
     *
     */
    public void draw()
    {
        _canvas.getGraphics().clearRect(0, 0, _canvas.getWidth(), _canvas.
                                        getHeight());
        _canvas.getGraphics().drawImage(ContentManager.
                getImage("background.png"), 0, 0, 400, 300, null);
        _player.draw(_canvas.getGraphics());
        _enemies.forEach((enemy) -> 
        {
            enemy.draw(_canvas.getGraphics());
        });
    }

    /**
     *
     * Actualizar el estado de los elementos del juego.
     *
     */
    public void update()
    {
        _player.update(_keyboardInput);
        _enemies.forEach((enemy) -> 
        {
            enemy.update();
        });
        _enemies.removeIf((enemy) -> !enemy.isAlive());
    }

    /**
     *
     * Método a ejecutar en el hilo.
     *
     */
    @Override
    public void run()
    {
        // Reproducir sonido de fondo.
        _soundThread = new Thread(new SoundPlayer("background.wav", true));
        _soundThread.start();

        double next_game_tick = System.currentTimeMillis();
        int loops;

        while (true)
        {
            loops = 0;
            while (System.currentTimeMillis() > next_game_tick
                    && loops < MAX_FRAMESKIP)
            {
                update();
                draw();

                next_game_tick += SKIP_TICKS;
                loops++;
            }
        }
    }

    // Getters
    /**
     *
     * Obtener el listado de enemigos.
     *
     * @return listado de enemigos en el juego.
     *
     */
    public ArrayList<Enemy> getEnemies()
    {
        return _enemies;
    }
}
