package spaceships;

import java.awt.Color;

public class Enemy extends Ship
{

    // 0: mover a la derecha
    // 1: mover a la izquierda
    private int _direction;

    // Indica si la bala se encuentra dentro del canvas y si no se ha chocado
    // con un enemigo.
    private boolean _isAlive;

    /**
     *
     * Crear una nueva instancia de enemigo.
     *
     * @param x         Posición inicial en el eje X.
     * @param y         Posición inicial en el eje Y.
     * @param width     Ancho de la nave en pixeles.
     * @param height    Alto de la nave en pixeles.
     * @param color     Color con el que se dibujará la nave enemiga.
     * @param imageName Nombre de la imagen que se dibujará.
     *
     */
    public Enemy(int x, int y, int width, int height, Color color,
                 String imageName)
    {
        super(x, y, width, height, color, imageName);
        _direction = 0;
        _isAlive = true;
    }

    /**
     *
     * Actualizar la posición del enemigo.
     *
     */
    @Override
    public void update()
    {
        // Validar que el enemigo se encuentre entre los limites del canvas,
        // si no es así, cambiar la dirección de movimiento.
        if (getX() == (400 - getWidth()))
        {
            _direction = 1;
        }
        else if (getX() == 0)
        {
            _direction = 0;
        }

        // Si el enemigo tiene direccion = 0, mover a la derecha, de lo contrario,
        // mover a la izquierda.
        if (_direction == 0)
        {
            setX(getX() + 1);
        }
        else
        {
            setX(getX() - 1);
        }
    }

    /**
     *
     * Comprobar si el enemigo aún vive.
     *
     * @return si el enemigo ha sido destruido o no.
     *
     */
    public boolean isAlive()
    {
        return _isAlive;
    }

    /**
     *
     * Cambiar la propiedad isAlive del enemigo a false para indicar que ha
     * sufrido un impacto con una bala.
     *
     * @param isAlive establece si se destruyo al enemigo.
     *
     */
    public void setIsAlive(boolean isAlive)
    {
        _isAlive = isAlive;
    }
}
