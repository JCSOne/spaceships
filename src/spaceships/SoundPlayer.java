package spaceships;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

public class SoundPlayer implements Runnable
{

    private Clip _clip;
    private final String _fileName;
    private final boolean _loop;

    public SoundPlayer(String fileName, boolean loop)
    {
        _fileName = fileName;
        _loop = loop;
        try
        {
            _clip = AudioSystem.getClip();
        }
        catch (LineUnavailableException ex)
        {
            ; // Ignored
        }
    }

    @Override
    public void run()
    {
        try
        {
            _clip.open(ContentManager.getSoundClip(_fileName));
            if (_loop)
            {
                _clip.loop(100);
            }
            else
            {
                _clip.loop(0);
            }
        }
        catch (LineUnavailableException | IOException ex)
        {
            ; // Ignored
        }
    }
}
